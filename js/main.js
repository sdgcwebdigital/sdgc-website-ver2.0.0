require.config({
	urlArgs: "bust=" + Math.random(),
	//baseUrl: 'js/libs',
	paths: {
		jquery: 'libs/jquery/jquery-3.2.1.min',
		//jqueryui: 'jquery/jquery-ui-1.10.4.min',
		bootstrap: 'libs/bootstrap/js/bootstrap.min',
		domready: 'libs/domReady',
		
		/* jquery plugins*/
		scrollTo: 'plugins/jquery.scrollTo',
		wow: 'plugins/wow.min',
		
	},
	waitSeconds: 0,
	shim: {
		jquery: {
            exports: '$'
        },
		bootstrap: {
            deps: ['jquery']
        },
		scrollTo: {
            deps: ['jquery']
        },
		wow: {
            deps: ['jquery']
        },
		
    }

});