var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css'); 
var autoprefixer = require('gulp-autoprefixer');


gulp.task('styles', function() {  
	gulp.src('scss/**/*.scss')
	.pipe(sass({
		includePaths: ['scss']
	}))
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(gulp.dest('css'));
});


// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('scss/*.scss', ['styles']);
});


// Default Task
gulp.task('default', ['styles', 'watch']);